const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const routes = require("./routes");

const port = process.env.PORT || 3001;
const app = express();

main().catch((err) => console.log(err));

async function main() {
    await mongoose.connect("mongodb://mongo:27017/todos", {
        useUnifiedTopology: true,
        useNewUrlParser: true,
    });

    app.use(cors());
    app.use(express.json());
    app.use("/api", routes);

    // Health check endpoint
    app.get("/health", (req, res) => res.status(200).send("OK"));
}

if (process.env.NODE_ENV !== "test") {
  app.listen(port, () => {
    console.log(`Server is listening on port: ${port}`);
  });
}

module.exports = app;

const request = require("supertest");
const chai = require("chai");
const expect = chai.expect;
const app = require("../server");

describe("Todos API", () => {
  it("should respond to the health check", async () => {
    const res = await request(app).get("/health");
    expect(res.status).to.equal(200);
    expect(res.text).to.equal("OK");
  });

  it("should get all todos", async () => {
    const res = await request(app).get("/api/todos");
    expect(res.status).to.equal(200);
    expect(res.body).to.be.an("array");
  });

  it("should create a new todo", async () => {
    const res = await request(app)
      .post("/api/todos")
      .send({
        title: "New Todo",
        description: "This is a new todo.",
        is_complete: false,
        due_date: new Date(),
      });
    expect(res.status).to.equal(200);
    expect(res.body.title).to.equal("New Todo");
  });

  it("should get a specific todo", async () => {
    const newTodo = await request(app)
      .post("/api/todos")
      .send({
        title: "Specific Todo",
        description: "This is a specific todo.",
        is_complete: false,
        due_date: new Date(),
      });
    const res = await request(app).get(`/api/todos/${newTodo.body._id}`);
    expect(res.status).to.equal(200);
    expect(res.body.title).to.equal("Specific Todo");
  });
});

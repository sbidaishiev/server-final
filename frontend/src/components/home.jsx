import { useEffect, useState } from "react";
import axios from "axios";
import {
  Button,
  Card,
  CardBody,
  CardTitle,
  ListGroup,
  ListGroupItem,
  Modal,
  ModalBody,
  ModalHeader,
} from "reactstrap";
import TodoForm from "./todo-form";
import "./Home.css";

const API_URL = process.env.REACT_APP_API_URL;

const Home = () => {
  const [todos, setTodos] = useState([]);
  const [modalOpen, setModalOpen] = useState(false);

  useEffect(() => {
    getTodos();
  }, []);

  const getTodos = async () => {
    try {
      const res = await axios.get(`${API_URL}/todos`);
      setTodos(res.data);
    } catch (err) {
      console.error("Error fetching todos:", err);
    }
  };

  const handleClick = async (id) => {
    try {
      await axios.patch(`${API_URL}/todos/${id}`, {
        is_complete: true,
      });
      await getTodos();
    } catch (err) {
      console.error("Error completing todo:", err);
    }
  };

  const handleNewTodo = async (todo) => {
    try {
      await axios.post(`${API_URL}/todos`, todo);
      await getTodos();
      setModalOpen(false);
    } catch (err) {
      console.error("Error creating todo:", err);
    }
  };

  return (
      <>
        <Card className="todo-card">
          <CardBody>
            <CardTitle tag="h1" className="todo-title">
              Todos
            </CardTitle>
            <ListGroup>
              {todos.map((todo) => (
                  <ListGroupItem
                      key={todo._id}
                      action
                      tag="a"
                      title="Click this to complete."
                      className={`todo-item ${todo.is_complete ? "completed" : ""}`}
                      onClick={() => handleClick(todo._id)}
                  >
                    <div className="d-flex w-100 justify-content-between">
                      <h5>{todo.title}</h5>
                      <small>Due: {new Date(todo.due_date).toLocaleDateString()}</small>
                    </div>
                    <p className="mb-1">{todo.description}</p>
                  </ListGroupItem>
              ))}
            </ListGroup>
            <Button onClick={() => setModalOpen(true)} color="primary" className="mt-3">
              Add Todo
            </Button>
          </CardBody>
        </Card>
        <Modal isOpen={modalOpen} toggle={() => setModalOpen(!modalOpen)}>
          <ModalHeader toggle={() => setModalOpen(!modalOpen)}>
            Add New Todo
          </ModalHeader>
          <ModalBody>
            <TodoForm saveTodo={handleNewTodo} />
          </ModalBody>
        </Modal>
      </>
  );
};

export default Home;

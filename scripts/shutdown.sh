#!/bin/bash
set -e

cd "$(dirname "$0")/.."

echo "Stopping and removing resources..."
docker-compose down -v
echo "Shutdown complete!"

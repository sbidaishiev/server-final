#!/bin/bash

set -e

cd "$(dirname "$0")/.."

echo "Deploying the web application..."
docker-compose up --build -d
echo "Deployment complete!"

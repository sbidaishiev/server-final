**`README.md`**
# Final Project: Dockerized Web Application Deployment with Bash Automation

## Overview
In this final project, We will design and implement a robust deployment pipeline for a web application using Bash scripting and Docker. The goal is to automate the deployment process, making it efficient, reproducible, and scalable.

### Key Components
1. **Web Application**:
   - A simple web application consisting of:
     - **Frontend**: ReactJS
     - **Backend**: Node.js with Express
     - **Database**: MongoDB

2. **Bash Scripts**:
   - Automate the deployment process using Bash scripting.

3. **Docker Containers**:
   - Utilize Docker to containerize the web application components.

4. **Deployment Pipeline**:
   - Automate the deployment process using Bash scripts and Docker Compose.

### Architecture Overview
A demonstration of Docker to implement a simple 3-tier architecture:
- **Frontend** will access the backend.
- **Backend** will access the database.

### Project Steps
1. **Project Planning**:
   - Define the architecture and plan the deployment process.

2. **Bash Scripting**:
   - Write Bash scripts for environment setup, dependency installation, and Docker tasks.

3. **Dockerization**:
   - Containerize web application components using Dockerfiles and Docker Compose.

4. **Deployment Pipeline**:
   - Develop a deployment pipeline that incorporates CI principles.

5. **Documentation**:
   - Provide clear documentation, including this README file.

6. **Demonstration**:
   - Prepare a demonstration of the deployment pipeline.

### Project Architecture Diagram
![Project Architecture Diagram](project-architecture.png)

### Usage
#### Prerequisites
- Docker
- Docker Compose

#### Running the Project
1. **Setup**: Run the setup script to install Docker and Docker Compose.

```bash
./scripts/setup.sh
```

2. **Deploy**: Run the deploy script to build and start the containers.

```bash
./scripts/deploy.sh
```

3. **Access Application**:
    - **Frontend**: [http://localhost:3000](http://localhost:3000)
    - **Backend**: [http://localhost:3001/api/todos](http://localhost:5000/api/todos)

#### Stopping and Removing Containers
Use the `shutdown.sh` script to stop and remove containers.

```bash
./scripts/shutdown.sh
```

#### Development and Testing
- **Frontend Development**:
    - Change to the `frontend` directory and start the development server.

  ```bash
  cd frontend
  npm start
  ```

- **Backend Development**:
    - Change to the `backend` directory and start the development server.

  ```bash
  cd backend
  npm start
  ```

### Directory Structure
```plaintext
server-final/
├── backend/
│   ├── server.js
│   ├── Dockerfile
│   ├── package.json
│   ├── routes/
│   │   ├── index.js
│   │   └── todos.js
│   ├── models/
│   │   └── todo.js
│   └── tests/
│       └── todos.test.js
├── frontend/
│   ├── Dockerfile
│   ├── package.json
│   └── src/
│       ├── App.jsx
│       ├── components/
│       │   ├── Home.jsx
│       │   └── TodoForm.jsx
│       └── index.js
├── scripts/
│   ├── deploy.sh
│   ├── setup.sh
│   └── shutdown.sh
├── docker-compose.yml
└── README.md
```

### Backend Code
**`backend/Dockerfile`**
```Dockerfile
FROM node:10-alpine

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3001

CMD ["node", "server.js"]
```

**`backend/package.json`**
```json
{
  "name": "docker-frontend-backend-db",
  "version": "1.0.0",
  "description": "A tiny web app",
  "main": "server.js",
  "scripts": {
    "start": "node server.js",
    "dev": "nodemon server.js",
    "test": "mocha --exit tests/**/*.test.js"
  },
  "author": "",
  "license": "ISC",
  "dependencies": {
    "cors": "^2.8.5",
    "express": "^4.17.1",
    "mongoose": "^5.9.18"
  },
  "devDependencies": {
    "mocha": "^9.0.0",
    "chai": "^4.3.4",
    "supertest": "^6.1.6"
  }
}
```

**`backend/routes/index.js`**
```js
const express = require("express");
const router = express.Router();

const todos = require('./todos');

router.get("/", (req, res) => {
  res.send("Main Page!");
});

router.use("/todos", todos);

module.exports = router;
```

**`backend/routes/todos.js`**
```js
router.get("/", async (req, res) => {...})

router.get("/:id", async (req, res) => {...})

router.post("/", async (req, res) => {...})

router.patch("/:id", async (req, res) => {...})

router.delete("/:id", async (req, res) => {...})

module.exports = router;
```

**`backend/models/Todo.js`**
```js
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

let Todo = new Schema({
    title: {
        type: String
    },
    description: {
        type: String
    },
    is_complete: {
        type: Boolean
    },
    due_date: {
        type: Date
    }
});

module.exports = mongoose.model('Todo', Todo);
```

### Frontend Code
**`frontend/Dockerfile`**
```Dockerfile
FROM node:14-alpine

WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH
COPY package*.json ./
RUN npm install
COPY . .

EXPOSE 3000
CMD ["npm", "start"]
```

**`frontend/package.json`**
```json
{
  "name": "frontend",
  "version": "0.1.0",
  "private": true,
  "dependencies": {
    "@testing-library/jest-dom": "^5.15.0",
    "@testing-library/react": "^11.2.7",
    "@testing-library/user-event": "^12.8.3",
    "axios": "^0.24.0",
    "bootstrap": "^5.1.3",
    "react": "^17.0.2",
    "react-dom": "^17.0.2",
    "react-scripts": "4.0.3",
    "reactstrap": "^9.0.0",
    "web-vitals": "^1.1.2"
  },
  "scripts": {
    "start": "react-scripts start",
    "build": "react-scripts build",
    "test": "react-scripts test",
    "eject": "react-scripts eject"
  },
  "eslintConfig": {
    "extends": [
      "react-app",
      "react-app/jest"
    ]
  },
  "browserslist": {
    "production": [
      ">0.2%",
      "not dead",
      "not op_mini all"
    ],
    "development": [
      "last 1 chrome version",
      "last 1 firefox version",
      "last 1 safari version"
    ]
  }
}
```

**`frontend/src/App.jsx`**
```jsx
import React from "react";
import Home from "./components/home";

import "./App.css";

function App() {
  return (
    <div className="container">
      <Home />
    </div>
  );
}

export default App;
```

**`frontend/src/components/Home.jsx`**
```jsx
import { useEffect, useState } from "react";
import axios from "axios";
import {
  Button,
  Card,
  CardBody,
  CardTitle,
  ListGroup,
  ListGroupItem,
  Modal,
  ModalBody,
  ModalHeader,
} from "reactstrap";
import TodoForm from "./todo-form";
import "./Home.css";

const API_URL = process.env.REACT_APP_API_URL;

const Home = () => {
  const [todos, setTodos] = useState([]);
  const [modalOpen, setModalOpen] = useState(false);

  useEffect(() => {
    getTodos();
  }, []);

  const getTodos = async () => {
    try {
      const res = await axios.get(`${API_URL}/todos`);
      setTodos(res.data);
    } catch (err) {
      console.error("Error fetching todos:", err);
    }
  };

  const handleClick = async (id) => {
    try {
      await axios.patch(`${API_URL}/todos/${id}`, {
        is_complete: true,
      });
      await getTodos();
    } catch (err) {
      console.error("Error completing todo:", err);
    }
  };

  const handleNewTodo = async (todo) => {
    try {
      await axios.post(`${API_URL}/todos`, todo);
      await getTodos();
      setModalOpen(false);
    } catch (err) {
      console.error("Error creating todo:", err);
    }
  };

  return (
      <>
        <Card className="todo-card">
          <CardBody>
            <CardTitle tag="h1" className="todo-title">
              Todos
            </CardTitle>
            <ListGroup>
              {todos.map((todo) => (
                  <ListGroupItem
                      key={todo._id}
                      action
                      tag="a"
                      title="Click this to complete."
                      className={`todo-item ${todo.is_complete ? "completed" : ""}`}
                      onClick={() => handleClick(todo._id)}
                  >
                    <div className="d-flex w-100 justify-content-between">
                      <h5>{todo.title}</h5>
                      <small>Due: {new Date(todo.due_date).toLocaleDateString()}</small>
                    </div>
                    <p className="mb-1">{todo.description}</p>
                  </ListGroupItem>
              ))}
            </ListGroup>
            <Button onClick={() => setModalOpen(true)} color="primary" className="mt-3">
              Add Todo
            </Button>
          </CardBody>
        </Card>
        <Modal isOpen={modalOpen} toggle={() => setModalOpen(!modalOpen)}>
          <ModalHeader toggle={() => setModalOpen(!modalOpen)}>
            Add New Todo
          </ModalHeader>
          <ModalBody>
            <TodoForm saveTodo={handleNewTodo} />
          </ModalBody>
        </Modal>
      </>
  );
};

export default Home;
```

**`frontend/src/components/TodoForm.jsx`**
```jsx
import React, { useState } from "react";
import { Button, Form, FormGroup, Input, Label } from "reactstrap";

const TodoForm = ({ saveTodo }) => {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [dueDate, setDueDate] = useState("");

  const onTitleChange = (e) => {
    setTitle(e.target.value);
  };

  const onDescriptionChange = (e) => {
    setDescription(e.target.value);
  };

  const onDueDateChange = (e) => {
    setDueDate(e.target.value);
  };

  const onSubmit = () => {
    saveTodo({ title, description, due_date: dueDate });
  };

  return (
    <Form>
      <FormGroup>
        <Label for="title">Title</Label>
        <Input
          id="title"
          name="title"
          placeholder="Enter a title"
          type="text"
          value={title}
          onChange={onTitleChange}
        />
      </FormGroup>
      <FormGroup>
        <Label for="description">Description</Label>
        <Input
          id="description"
          name="description"
          placeholder="Enter a Description"
          type="textarea"
          value={description}
          onChange={onDescriptionChange}
        />
      </FormGroup>
      <FormGroup>
        <Label for="duedate">Due Date</Label>
        <Input
          id="duedate"
          name="duedate"
          type="date"
          value={dueDate}
          onChange={onDueDateChange}
        />
      </FormGroup>
      <Button color="primary" onClick={onSubmit}>Save</Button>
    </Form>
  );
};
export default TodoForm;
```

### Bash Scripts
**`scripts/setup.sh`**
```bash
#!/bin/bash
set -e

if ! [ -x "$(command -v docker)" ]; then
  echo "Installing Docker..."
  curl -fsSL https://get.docker.com -o get-docker.sh
  sh get-docker.sh
  rm get-docker.sh
fi

if ! [ -x "$(command -v docker-compose)" ]; then
  echo "Installing Docker Compose..."
  sudo curl -L "https://github.com/docker/compose/releases/download/v2.12.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
  sudo chmod +x /usr/local/bin/docker-compose
fi

if ! groups $USER | grep &>/dev/null "\bdocker\b"; then
  sudo usermod -aG docker $USER
  echo "Added $USER to docker group. Please log out and back in."
fi

echo "Setup complete!"
```

**`scripts/deploy.sh`**
```bash
#!/bin/bash

set -e

cd "$(dirname "$0")/.."

echo "Deploying the web application..."
docker-compose up --build -d
echo "Deployment complete!"
```

**`scripts/shutdown.sh`**
```bash
#!/bin/bash
set -e

cd "$(dirname "$0")/.."

echo "Stopping and removing resources..."
docker-compose down -v
echo "Shutdown complete!"
```

### Docker Compose File
**`docker-compose.yml`**
```yaml
version: "3.3"
services:
  web:
    build: frontend
    image: web:latest
    depends_on:
      - api
    ports:
      - "${WEB_PORT:-3000}:3000"
    restart: always
    networks:
      - network-backend

  api:
    build: backend
    image: api:latest
    environment:
      - NODE_ENV=${NODE_ENV}
      - PORT=${API_PORT}
    depends_on:
      - mongo
    ports:
      - "${API_PORT}:${API_PORT}"
    restart: always
    networks:
      - network-backend
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:${API_PORT}/health"]
      interval: 30s
      timeout: 10s
      retries: 3

  mongo:
    image: mongo:latest
    restart: always
    volumes:
      - mongodb_data:/data/db
    environment:
      - MONGODB_INITDB_ROOT_USERNAME=${MONGO_INITDB_ROOT_USERNAME}
      - MONGODB_INITDB_ROOT_PASSWORD=${MONGO_INITDB_ROOT_PASSWORD}
    networks:
      - network-backend
    healthcheck:
      test: ["CMD", "mongo", "--eval", "db.runCommand('ping').ok"]
      interval: 30s
      timeout: 10s
      retries: 3

networks:
  network-backend:

volumes:
  mongodb_data:
```

### Evaluation Criteria
1. **Automation Completeness (20%)**:
    - Evaluate the extent to which Bash scripts automate the deployment process.

2. **Containerization Best Practices (20%)**:
    - Assess adherence to Docker best practices.

3. **Deployment Pipeline Efficiency (20%)**:
    - Review the effectiveness of the deployment pipeline.

4. **Documentation Quality (20%)**:
    - Evaluate the clarity of project documentation.

5. **Presentation and Demonstration (20%)**:
    - Assess the quality of the presentation.

### Demonstration
- **Deployment Pipeline Demonstration Video**:
  [Deployment Pipeline Video](deployment-demo.mp4)

### Authors and acknowledgment
- [Namazbek](https://gitlab.com/namazbekzhan)
- [Shyntas](https://gitlab.com/sbidaishiev)
- [Gauhar](https://github.com/gauharina044444)
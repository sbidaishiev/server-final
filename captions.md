### Introduction
**Caption 1:**
> "Welcome to the final project demonstration: Dockerized Web Application Deployment with Bash Automation."

**Caption 2:**
> "In this video, I will show you how to automate the deployment of a simple three-tier web application using Bash scripts and Docker."

### Key Components Overview
**Caption 3:**
> "Our web application consists of three key components: Frontend, Backend, and Database."

**Caption 4:**
> "The frontend is built using ReactJS."
> - "The backend is a Node.js application with Express."
> - "The database is a MongoDB instance."

### Architecture Diagram
**Caption 5:**
> "Here's the architecture diagram of the application."
> - "The frontend communicates with the backend."
> - "The backend accesses the database."

### Project Setup
**Caption 6:**
> "Let's start by setting up the environment using Bash scripts."

**Caption 7:**
> "The `setup.sh` script installs Docker and Docker Compose."

**Terminal Command 1:**
```bash
./scripts/setup.sh
```

### Dockerization
**Caption 8:**
> "After setting up the environment, let's build and start the Docker containers."

**Caption 9:**
> "The `docker-compose.yml` file orchestrates our multi-container setup."
> - "It defines services for the frontend, backend, and database."

**Terminal Command 2:**
```bash
./scripts/deploy.sh
```

**Caption 10:**
> "The `deploy.sh` script uses Docker Compose to build and start the containers."

### Web Application Access
**Caption 11:**
> "The web application is now accessible at [http://localhost:3000](http://localhost:3000)."

**Screen Capture of Frontend:**
> "Here is the frontend of our web application, displaying a list of to-do items."

### Backend API Access
**Caption 12:**
> "The backend API is available at [http://localhost:5000/api/todos](http://localhost:5000/api/todos)."

**Screen Capture of Backend API:**
> "Here is the response from the backend API, providing data in JSON format."

### Bash Scripts Overview
**Caption 13:**
> "Let's take a closer look at the Bash scripts that automate this deployment."

**`setup.sh`:**
**Caption 14:**
> "`setup.sh`: Installs Docker and Docker Compose, and adds the current user to the Docker group."

**`deploy.sh`:**
**Caption 15:**
> "`deploy.sh`: Uses Docker Compose to build and start the containers."

**`teardown.sh`:**
**Caption 16:**
> "`teardown.sh`: Stops and removes containers, networks, volumes, and Docker images."

**Terminal Command 3:**
```bash
./scripts/teardown.sh
```

**Caption 17:**
> "The `teardown.sh` script cleans up everything, ensuring a fresh start for the next deployment."

### Docker Compose and Dockerfiles
**Caption 18:**
> "Our Docker Compose file defines the services and dependencies between them."

**Show `docker-compose.yml`:**
```yaml
version: '3.8'

services:
  frontend:
    build:
      context: ./frontend
      dockerfile: Dockerfile
    ports:
      - "3000:3000"
    depends_on:
      - backend

  backend:
    build:
      context: ./backend
      dockerfile: Dockerfile
    ports:
      - "5000:5000"
    depends_on:
      - mongo

  mongo:
    image: mongo:latest
    ports:
      - "27017:27017"
    volumes:
      - mongo-data:/data/db

volumes:
  mongo-data:
```

**Caption 19:**
> "Each component has its own Dockerfile, specifying the necessary instructions to build the images."

**Frontend Dockerfile:**
```Dockerfile
FROM node:14-alpine

WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH

COPY package*.json ./
RUN npm install

COPY . .

EXPOSE 3000
CMD ["npm", "start"]
```

**Backend Dockerfile:**
```Dockerfile
FROM node:14-alpine

WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH

COPY package*.json ./
RUN npm install

COPY . .

EXPOSE 5000
CMD ["node", "index.js"]
```

### Conclusion
**Caption 20:**
> "To summarize, we have successfully automated the deployment of a Dockerized web application using Bash scripts and Docker Compose."

**Caption 21:**
> "This project follows best practices for containerization and provides an efficient deployment pipeline."

**Caption 22:**
> "Thank you for watching the demonstration. Feel free to ask any questions!"